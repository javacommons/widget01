#include "widget.h"
#include "ui_widget.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "common.h"

#include "msys2dialog.h"

static QString np(QString x)
{
    return x.replace("/", "\\");
}

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            SLOT(showContextMenuForListWidget1(const QPoint &)));
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            SLOT(showContextMenuForListWidget2(const QPoint &)));
    this->ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove); // Drag & Drop Item Sort
    this->reloadNameList();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::reloadNameList()
{
    MySettings settings;

    ui->listWidget->clear();
    QString docs = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    // ファイルフィルタ
    QDir::Filters filters = QDir::Dirs;
    // 対象フラグ
    QDirIterator::IteratorFlags flags = QDirIterator::NoIteratorFlags;
    // イテレーターの生成
    QDirIterator it(docs, filters, flags);
    int found = -1;
    QString selectedRepoName = settings.value(settings.SELECTED_REPO_NAME).toString();
    qDebug() << "selectedRepoName=" << selectedRepoName;
    while (it.hasNext())
    {
        QString dir = it.next();
        if(dir.endsWith("/.") || dir.endsWith("/..")) continue;
        if(!QDir(dir + "/.git").exists()) continue;
        qDebug() << dir;
        qDebug() << QFileInfo(dir).fileName();
        ui->listWidget->addItem(QFileInfo(dir).fileName());
        if(QFileInfo(dir).fileName()==selectedRepoName)
        {
            found = ui->listWidget->count() - 1;
        }
    }
    if(found >= 0)
    {
        ui->listWidget->item(found)->setSelected(true);
    }

    ui->listWidget_2->clear();
    QString user = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString msys2 = user + "/.software/msys2";
    qDebug() << msys2;
    // イテレーターの生成
    QDirIterator it2(msys2, filters, flags);
    int found2 = -1;
    QString selectedMsys2Name = settings.value(settings.SELECTED_MSYS2_NAME).toString();
    qDebug() << "selectedMsys2Name=" << selectedMsys2Name;
    while (it2.hasNext())
    {
        QString dir = it2.next();
        qDebug() << dir;
        if(dir.endsWith("/.") || dir.endsWith("/..")) continue;
        if(!QFile(dir + "/msys2_shell.cmd").exists()) continue;
        qDebug() << dir;
        qDebug() << QFileInfo(dir).fileName();
        ui->listWidget_2->addItem(QFileInfo(dir).fileName());
        if(QFileInfo(dir).fileName()==selectedMsys2Name)
        {
            found2 = ui->listWidget_2->count() - 1;
        }
        if(found2 >= 0)
        {
            ui->listWidget_2->item(found2)->setSelected(true);
        }
    }
}

void Widget::showContextMenuForListWidget1(const QPoint &pos)
{
    const QPoint localPos = ui->listWidget->mapFrom(this, pos);
    QModelIndex index = ui->listWidget->indexAt(localPos);
    int row = index.row();
    qDebug() << "row=" << index.row();
    if(index.row() < 0) return;
    QListWidgetItem *item = ui->listWidget->item(row);
    QMenu contextMenu("Context menu", this);
    QAction *actRemove = new QAction(QString("%1 を削除").arg(item->text()), this);
    connect(actRemove, &QAction::triggered, [this, item]()
    {
        QMessageBox::information(this, "title", item->text());
    });
    contextMenu.addAction(actRemove);
    contextMenu.exec(mapToGlobal(pos));
}

void Widget::showContextMenuForListWidget2(const QPoint &pos)
{
    const QPoint localPos = ui->listWidget_2->mapFrom(this, pos);
    QModelIndex index = ui->listWidget_2->indexAt(localPos);
    int row = index.row();
    qDebug() << "row=" << index.row();
    if(index.row() < 0) return;
    QListWidgetItem *item = ui->listWidget_2->item(row);
    QMenu contextMenu("Context menu", this);
    QAction *actNyagos = new QAction(QString("%1 を nyagos で起動").arg(item->text()), this);
    connect(actNyagos, &QAction::triggered, [this, item]()
    {
        //QMessageBox::information(this, "nyagos", item->text());
        this->on_pushButton_clicked();
    });
    contextMenu.addAction(actNyagos);
    QAction *actBash = new QAction(QString("%1 を bash で起動").arg(item->text()), this);
    connect(actBash, &QAction::triggered, [this, item]()
    {
        //QMessageBox::information(this, "bash", item->text());
        this->on_pushButton_2_clicked();
    });
    contextMenu.addAction(actBash);
    contextMenu.exec(mapToGlobal(pos));
}

void Widget::on_pushButton_clicked()
{
    QString prof = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString docs = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    auto uhomeItem = ui->listWidget->currentItem();
    if(uhomeItem == nullptr) return;
    QString uhome = docs + "/" + uhomeItem->text();
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QString path = env.value("PATH");
    QString dir = qApp->applicationDirPath();
    QString cmd1 = dir + "/cmd";
    QString cmd2 = uhome + "/cmd";
    QString pathAdded = np(cmd1) + ";" + np(cmd2);
    auto msys2Item = ui->listWidget_2->currentItem();
    if(msys2Item != nullptr)
    {
        qDebug() << "(msys2Item != nullptr)";
        pathAdded += ";";
        pathAdded += np(prof + "/.software/msys2/" + msys2Item->text());
    }
    qDebug() << "pathAdded=" << pathAdded;
    QProcess proc;
    env.insert("HOME", np(uhome));
    env.insert("PATH", pathAdded + ";" + path);
    proc.setProgram(R"(wt.exe)");
    proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(uhome));
    proc.setArguments(QStringList() << R"(nt)" << R"(nyagos.exe)");
    qDebug() << proc.startDetached();
    this->showMinimized();
}


void Widget::on_pushButton_2_clicked()
{
    QString prof = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString docs = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    auto uhomeItem = ui->listWidget->currentItem();
    if(uhomeItem == nullptr) return;
    QString uhome = docs + "/" + uhomeItem->text();
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QString path = env.value("PATH");
    QString dir = qApp->applicationDirPath();
    QString cmd1 = dir + "/cmd";
    QString cmd2 = uhome + "/cmd";
    QString pathAdded = np(cmd1) + ";" + np(cmd2);
    auto msys2Item = ui->listWidget_2->currentItem();
    if(msys2Item != nullptr)
    {
        pathAdded += ";";
        pathAdded += np(prof + "/.software/msys2/" + msys2Item->text());
    }
    qDebug() << dir;
    qDebug() << QDir(dir).exists();
    QProcess proc;
    env.insert("HOME", np(uhome));
    env.insert("PATH", pathAdded + ";" + path);
    //proc.setProgram(R"(wt.exe)");
    proc.setProgram(R"(cmd.exe)");
    proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(uhome));
    //proc.setArguments(QStringList() << R"(nt)" << R"(nyagos.exe)" << R"(-k)" << R"(bbb.cmd)");
    //proc.setArguments(QStringList() << R"(nt)" << "busybox.exe" << "bash" << "-l");
    proc.setArguments(QStringList() << "/c" << "start" << "busybox.exe" << "bash" << "-l");
    qDebug() << proc.startDetached();
    this->showMinimized();
}

static void runCmd(QString cmdLines, QString workDir, QString title="") {
    QString dir = qApp->applicationDirPath().replace("/", "\\");
    QString temp = dir + "\\temp";
    qDebug() << temp;
    QDateTime dt = QDateTime::currentDateTime();
    QTemporaryFile tempFile(temp + "\\" + dt.toString("yyyy-MM-dd-hh-mm-ss") + "-XXXXXX.cmd");
    tempFile.setAutoRemove(false);
    if(tempFile.open())
    {
        qDebug() << tempFile.fileName();
        tempFile.write(cmdLines.toLocal8Bit());
        tempFile.close();
        QString userProfile = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
        userProfile = userProfile.replace("/", "\\");
        qDebug() << "userProfile=" << userProfile;
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        QString path = env.value("PATH");
        QString dir = qApp->applicationDirPath();
        dir = dir.replace("/", "\\") + "\\cmd";
        qDebug() << dir;
        qDebug() << QDir(dir).exists();
        QProcess proc;
        env.insert("HOME", temp);
        env.insert("PATH", dir + ";" + path);
        proc.setProgram(R"(wt.exe)");
        proc.setProcessEnvironment(env);
        proc.setWorkingDirectory(workDir);
        if(title.isEmpty())
        {
            proc.setArguments(QStringList() << R"(nt)" << R"(nyagos.exe)" << R"(-c)" << tempFile.fileName());
        }
        else
        {
            proc.setArguments(QStringList() << R"(nt)" << R"(--title)" << title << R"(nyagos.exe)" << R"(-c)" << tempFile.fileName());
        }
        qDebug() << proc.startDetached();
    }
    if(tempFile.open())
    {
        QTextStream out(&tempFile);
        //out.setCodec("UTF-8");
        //out.setCodec("cp932");
        qDebug().noquote() << out.readAll();
        tempFile.close();
    }
}

void Widget::on_pushButton_3_clicked()
{
    QString docs = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("git config --system core.autocrlf input") << Qt::endl;
    strm << QString("git config --system core.autocrlf") << Qt::endl;
    strm << QString("git config --show-origin core.autocrlf") << Qt::endl;
    strm << QString("if not exist flutter git clone https://github.com/flutter/flutter.git -b stable") << Qt::endl;
    strm << QString("envset prepend PATH \"%1\"").arg(np(docs + "/flutter/bin")) << Qt::endl;
    strm << QString("call \"%1\" doctor").arg(np(docs + "/flutter/bin/flutter.bat")) << Qt::endl;
    strm << QString("call \"%1\" upgrade").arg(np(docs + "/flutter/bin/flutter.bat")) << Qt::endl;
    strm << QString("call \"%1\" doctor").arg(np(docs + "/flutter/bin/flutter.bat")) << Qt::endl;
    strm << QString("pause") << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    docs = docs.replace("/", "\\");
    runCmd(cmdLines, docs);
}

QString enclosePath(QString x)
{
    return "\"" + x + "\"";
}

// install msys2
void Widget::on_pushButton_4_clicked()
{
    Msys2Dialog dlg;
    if(!dlg.exec()) return;
    //QMessageBox::information(this, "title", dlg.name());
    QString msys2Name = dlg.name();
    QString userProfile = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString dir = qApp->applicationDirPath();
    QString archive = "msys2-base-x86_64-20220319.tar.xz";
    QString archive_file = np(QString("%1/%2").arg(qApp->applicationDirPath() + "/temp").arg(archive));
    QString archive_url = QString("https://gitlab.com/javacommons/widget01/-/raw/main/%1").arg(archive);
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("if not exist %1 busybox wget -O %1 %2").arg(archive_file).arg(archive_url) << Qt::endl;
    strm << QString("if not exist .software\\msys2 mkdir .software\\msys2") << Qt::endl;
    strm << QString("cd .software\\msys2") << Qt::endl;
    strm << QString("if exist %1 rmdir /s /q %1").arg(msys2Name) << Qt::endl;
    strm << QString("mkdir %1").arg(msys2Name) << Qt::endl;
    strm << QString("busybox tar xvf %2 -C %1 --strip-components 1").arg(msys2Name).arg(archive_file) << Qt::endl;
    strm << QString("call %1\\msys2_shell.cmd -msys2 -defterm -here -no-start -c %2").arg(msys2Name).arg(QString("echo \"Installation Complete (%1)...\"").arg(msys2Name)) << Qt::endl;
    strm << QString("cd %1\\etc").arg(msys2Name) << Qt::endl;
    strm << QString("if not exist bash.bashrc.orig copy bash.bashrc bash.bashrc.orig") << Qt::endl;
    strm << QString("busybox sed -e \"s/^  export PS1=.*$/  export PS1='%1 ($MSYSTEM) \\\\w \\$ '/g\" bash.bashrc.orig > bash.bashrc").arg(msys2Name) << Qt::endl;
    strm << QString("pause") << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    qDebug().noquote() << cmdLines;
    runCmd(cmdLines, userProfile, QString("Installing %1...").arg(msys2Name));
}

void Widget::on_pushButton_5_clicked()
{
    if(ui->listWidget->currentRow() < 0) return;
    MySettings settings;
    qDebug() << settings.fileName();
    settings.setValue(settings.SELECTED_REPO_NAME, ui->listWidget->currentItem()->text());
}


void Widget::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(current->text().isEmpty()) return;
    MySettings settings;
    qDebug() << settings.fileName();
    settings.setValue(settings.SELECTED_REPO_NAME, current->text());
}


void Widget::on_listWidget_2_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(current->text().isEmpty()) return;
    MySettings settings;
    qDebug() << settings.fileName();
    settings.setValue(settings.SELECTED_MSYS2_NAME, current->text());
}

