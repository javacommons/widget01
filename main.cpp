#include "widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto font = a.font();
    qDebug() << font.pointSizeF();
    font.setPointSizeF(12.0);
    a.setFont(font);
    Widget w;
    w.show();
    return a.exec();
}
