#ifndef COMMON_H
#define COMMON_H

#include <QtCore>

class MySettings: public QSettings
{
public:
    //MySettings(QObject *parent = nullptr): QSettings("javacommons", "widget01", parent)
    MySettings(): QSettings(QSettings::IniFormat, QSettings::UserScope,
                                                     "javacommons", "widget01")
    {
    }
    const QString SELECTED_REPO_NAME = "selectedRepoName";
    const QString SELECTED_MSYS2_NAME = "selectedMsys2Name";
};

#endif // COMMON_H
