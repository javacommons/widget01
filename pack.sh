#! bash -uvx
#mingw -c "mk.mgw widget01"
mk.cmd widget01
./busybox-x86_64.exe --install ./cmd
cp -p ./busybox-x86_64.exe ./cmd/busybox.exe
rm -rf cmd/ar.exe cmd/strings.exe
ts=`date "+%Y.%m.%d.%H.%M.%S"`
rm -rf widget01-*.7z
7z a -t7z widget01-$ts.7z widget01-x86_64-static.exe cmd temp -x!*.user -x!temp/*
sha256sum widget01-$ts.7z
sum1=`sha256sum widget01-$ts.7z | awk '{print $1}'`
echo $sum1
#tar=`ls msys2-base-*.tar.xz`
#sum2=`sha256sum $tar | awk '{print $1}'`
cat << EOS > widget01.json
{
    "version": "$ts",
    "description": "",
    "homepage": "",
    "license": "MIT",
    "depends": [
        "main/nyagos",
        "extras/windows-terminal"
    ],
    url: [
        "https://gitlab.com/javacommons/widget01/-/raw/main/widget01-$ts.7z"
    ],
    "hash": [
        "$sum1"
    ],
    "shortcuts": [
        [
            "widget01-x86_64-static.exe",
            "Widget01",
            "--dummy"
        ]
    ]
}
EOS
git add .
git commit -m.
git push
